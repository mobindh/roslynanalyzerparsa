﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoslynAnalyzer.CleanCodeAnalyzers.Data
{
    class IssueDetail
    {
        public int id;
        public string quote;
        public string author;
        public string title;
        const string BOB = "Robert C. Martin";

        public IssueDetail(int id, string quote, string author, string title)
        {
            this.id = id;
            this.quote = quote;
            this.author = author;
            this.title = title;
        }

        public static IssueDetail SingleReturnFunctions = new IssueDetail(
            id: 1,
            quote: "A function should do one thing, and only one thing.",
            author: BOB,
            title: "Your code has methods with multiple returns in these methods:"
            );

        public static IssueDetail BooleanParameter = new IssueDetail(
            id: 2,
            quote: "The main issue with a method which has a boolean parameter is that it forces the method body to handle logic it should have been told about.",
            author: BOB,
            title: "Your code has methods with boolean parameters in here:"
            );
        public static IssueDetail FunctionArgCounters = new IssueDetail(
            id: 3,
            quote: "The ideal number of arguments for a function is zero(nomadic), Next comes one(monadic), followed closely by two (dyadic). Three arguments(triadic) should be avoided where possible. More than three(polyadic) requires very special justification — and then shouldn’t be used anyway.",
            author: BOB,
            title: "Your code has methods with boolean parameters in here:"
            );
        public static IssueDetail OneLineBranchStatements = new IssueDetail(
            id: 4,
            quote: "This implies that the blocks within if statements, _else_ statements, _while_ statements, and so on should be one line long.",
            author: BOB,
            title: "Your code has methods with boolean parameters in here:"
            );

        public static IssueDetail NestedLoop = new IssueDetail(
            id: 5,
            quote: "Nested loops are frequently (but not always) bad practice.",
            author: BOB,
            title: "Nested loops were found:"
            );

        public static IssueDetail TryCatchFucntionCalls = new IssueDetail(
            id: 6,
            quote: "Try/Catch blocks are ugly in their own right. They confuse the structure of the code and mix error processing with normal processing. So it is better to extract the bodies of the try and catch blocks out into functions of their own.",
            author: BOB,
            title: "Your code has methods with boolean parameters in here:"
            );

        public static IssueDetail SingleAssertPerTest = new IssueDetail(
            id: 7,
            quote: "This is a quote 3",
            author: BOB,
            title: "You have test methods that assert more than one thing:");
       
        public static IssueDetail CodeCommentPercentage = new IssueDetail(
            id: 8,
            quote: " It is well known that I prefer code that has few comments. I code by the principle that good code does not require many comments",
            author: BOB,
            title: ""
            );
        public static IssueDetail StaticVoid = new IssueDetail(
            id: 9,
            quote: "Better to not declare static methods as void",
            author: BOB,
            title: ""
            );

        public static IssueDetail MagicNumbers = new IssueDetail(
            id: 10,
            quote: "Replace Magic Numbers with Named Constants",
            author: BOB,
            title: "Your code had magic numbers in the following locations:"
            );

        public static IssueDetail LongClasses = new IssueDetail(
            id: 11,
            quote: "As with functions, smaller is the primary rule when it comes to designing classes.",
            author: BOB,
            title: "Long classes were detected:"
        );

        public static IssueDetail EmptyCatches = new IssueDetail(
            id: 12,
            quote: "it's usually a very bad idea to have an empty catch block",
            author: BOB,
            title: "Empty catches were detected:");

        public static IssueDetail NestedConditions = new IssueDetail(
            id: 13,
            quote: "Do not nest your code beyond limits",
            author: BOB,
            title: "Nested if statements detected:"
            );
        public static IssueDetail LongLines = new IssueDetail(
            id: 14,
            quote: "The code should be read vertically not horizontally. Therefore Long horizontal lines of code should be avoided.",
            author: BOB,
            title: "Long lines were detected:"
            );
        public static IssueDetail LongMethods = new IssueDetail(
            id: 15,
            quote: "Methods should be small, smaller than small.",
            author: BOB,
            title: "Long methods were detected:"
        );

    }
}
